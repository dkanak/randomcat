//
//  main.m
//  RandomCat
//
//  Created by Damian Kanak on 07/01/2015.
//  Copyright (c) 2015 Mokan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
